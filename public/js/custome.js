
$(document).ready(function(){

// const socket = io();    
// const socket = io.connect('http://localhost:3000');

var i = 0;
window.Echo.channel('tasks-list')
 .listen('.TaskEvent', (data) => {
    i++;
    var HTML = `
        <tr>
            <th scope="row">${data.id}</th>
            <td>${data.name}</td>
            <td>${data.description}</td>
            <td>${data.date}</td>
        </tr>    
    `;
    $("#tbody-tasks").append(HTML);
});


$('#datepicker').datepicker({
    weekStart: 1,
    daysOfWeekHighlighted: "6,0",
    autoclose: true,
    todayHighlight: true,
});
    

$('#CustomerForm').validate({
        rules: {
            'checkbox': {
                required: true
            }
        },
        highlight: function (input) {
            $(input).addClass('is-invalid');
        },
        unhighlight: function (input) {
            $(input).removeClass('is-invalid');
        },
        errorPlacement: function (error, element) {
            $(element).next().append(error);
        },
        submitHandler: function(form) {
            saveData();
        }
    });
    function saveData(){
        $(this).html('Sending..');
        $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#CustomerForm').serialize(),
            url: "task/storeTask",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#CustomerForm').trigger("reset");
                // $('#ajaxModel').modal('hide');
                // table.draw();
                $('.flash-message').show();
                $('.flash-message #msg').html(data.success);
                // socket.emit('createTask', data.data);
                $('.flash-message').delay(2000).fadeOut('slow');

            },
            error: function (data) {
                console.log('Error:', data);
                $('#saveBtn').html('Save Changes');
            }
        });
    };
});