FROM docker.io/bitnami/laravel:8

# Memory Limit
USER root
RUN chmod 777 /app
RUN echo "memory_limit=1024M" >> /opt/bitnami/php/etc/php.ini