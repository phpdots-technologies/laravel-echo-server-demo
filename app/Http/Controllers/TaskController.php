<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{

    /**
     * Store a newly created task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'date' => 'required'
        ], [
            'name.required' => 'Task is required',
            'description.required' => 'Description is required',
            'date.required' => 'Date is required'
        ]);
        $task = Task::create($validatedData);
        event(new \App\Events\TaskCreated($task->toArray()));
        return response()->json(['success'=>'Task created successfully!','data' => $task->latest()->first()]);
    }    
    /**
     *  Display a listing of the taks..
     *
     * @return \Illuminate\View\View
     */
    public function show()
    {
        $data = $this->getTask();
        return view('task/task', ['task'=>$data]);
    }

      /**
     * Get all listing of the taks.
     *
     * @return array
     */
    protected function getTask(){
        $data = Task::get();
        return $data;
    }
}