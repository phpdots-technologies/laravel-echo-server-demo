<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 */
class Task extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'date', 'created_at', 'updated_at'];

}
