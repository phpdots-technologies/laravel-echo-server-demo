@extends('layouts.master')
@section('title', 'Page Title')
@section('sidebar')  
@stop
@section('content')
<div class="bd-example">
        <div class="row">
        <form id="CustomerForm" name="CustomerForm" class="row gx-3 gy-2 align-items-center">
            <div class="col">
                <input type="text" class="form-control" name="name" placeholder="Taks" id="name" class="form-control"  autocomplete='name' value="" required>
                <div class="invalid-feedback"></div>
                @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
            </div>
            <div class="col">
                <input type="text" class="form-control" name="description" placeholder="Description" id="description" class="form-control"  autocomplete='description' value="" required>
                <div class="invalid-feedback"></div>
                @if ($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
            </div>
            <div class="col">
                <input data-date-format="yyyy-mm-dd" id="datepicker" type="text" class="form-control" name="date" placeholder="Date" id="date" class="form-control" autocomplete='off' value="" required>
                <div class="invalid-feedback"></div>   
                @if ($errors->has('date'))
                    <span class="text-danger">{{ $errors->first('date') }}</span>
                @endif         
            </div>
            <div class="col">
                <button class="btn btn-primary" id="saveBtn">
                        Submit
                </button>
            </div>
            </form>
        </div>
    <div class="row">
    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Task</th>
      <th scope="col">Description</th>
      <th scope="col">Date</th>
    </tr>
  </thead>
  <tbody id="tbody-tasks">
    @foreach ($task as $task)
    <tr>
        <th scope="row">{{ $task->id }}</th>
        <td>{{ $task->name }}</td>
        <td>{{ $task->description }}</td>
        <td>{{ $task->date }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</div>
@stop
