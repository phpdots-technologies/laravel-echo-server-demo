<!doctype html>
<html>
<head>
   @include('includes.head')
</head>
<body>
<div class="container">
   <header class="row">
       @include('includes.header')
   </header>
   <div class="container">
        <div id="main" class="row">
        <div class="alert alert-success alert-dismissible fade show flash-message" style="display: none;">
                <div id="msg"><strong>Success!</strong> Your message has been sent successfully.</div>
                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
            </div> 
                @yield('content')
        </div>
   </div>
   <footer class="my-5 pt-5 text-muted text-center text-small">
       @include('includes.footer')
   </footer>
</div>
</body>
    <script>
            window.laravel_echo_port='{{env("LARAVEL_ECHO_PORT")}}';
    </script>
</html>