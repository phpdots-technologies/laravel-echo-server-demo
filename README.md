## About The Project
* created a task form with following fields (Name, Description, Date)
* Added validation
* Saved data to db
* Show added tasks above form
* Updated tasks for all users viewing the page in realtime using websockets.


### Built With
* [Laravel](https://laravel.com) (Front-End/Back-End)
* [Laravel Echo Server](https://www.npmjs.com/package/laravel-echo-server)

### Installation
1. Clone the repo in your local
   ```sh
   git clone https://bitbucket.org/phpdots-technologies/laravel-echo-server-demo/src/master/
   cd master
   ```
2. Install Docker
   ```sh
    Linux - https://docs.docker.com/linux/started/
    Mac - http://docs.docker.com/mac/started/
    Windows - http://docs.docker.com/windows/started/
    ```
3. Open command promot and run below command
   ```sh
   docker-compose build --no-cache
   ```
4. After complete build run below command
   ```sh
   docker-compose up
   ```
Now your application install successfully and ready to use. used these ports in app: 3000,3306,6001,6379,8080. make sure these ports not used with any other server in machine.

* App URL: http://localhost:3000
* Check PHP memory limit: http://localhost:3000/info
* phpMyAdmin URL: http://localhost:8080/